@push('menus')


<li class="nav-item">
    <a href="{{ route('home') }}" class="nav-link home">
        <i class="nav-icon fas fa-home"></i>
        <p>
            Dashboard
            {{-- <span class="right badge badge-danger">New</span> --}}
        </p>
    </a>
</li>
<li class="nav-item has-treeview m_func">
    <a href="#" class="nav-link func">
        <i class="nav-icon fas fa-users"></i>
        <p>
            Funcionarios
            <i class="fas fa-angle-left right"></i>
        </p>
    </a>
    <ul class="nav nav-treeview">
        <li class="nav-item">
            <a href="{{ route('employees.create') }}" class="nav-link create_func">
                <i class="fas fa-plus-circle nav-icon"></i>
                <p>Adicionar Funcionarios</p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ route('employees.index') }}" class="nav-link index_func">
                <i class="fas fa-list nav-icon"></i>
                <p>Listar Funcionarios</p>
            </a>
        </li>

    </ul>
</li>


@endpush
@push('script')

<script>
    @foreach ($atives as $ative)
        $('.' + '{{ $ative }}').addClass('active')
    @endforeach
    @foreach ($menus as $ative)
    $('.' + '{{ $ative }}').addClass('menu-open')
    @endforeach
</script>
@endpush
