@extends('layouts.admin')



@section('content')

<div class="main-card mb-3 card">
    <div class="card-body">
       <h5>Fucionario</h5>
        <form class="needs-validation" novalidate>
            <fieldset disabled="disabled">
            <div class="form-row">
                <div class="col-md-4 mb-3">
                    <label for="validationCustom01">Nome Completo</label>
                <input type="text" class="form-control" id="validationCustom01" value="{{$employee->nome}}" name="nome_completo" placeholder="Nome Completo" required>
                 </div>
                <div class="col-md-4 mb-3">
                    <label for="validationCustom02">Apelido</label>
                    <input type="text" class="form-control" id="validationCustom02" value="{{$employee->apelido}}"  name ="apelido" placeholder="Apelido" required>
                </div>
                <div class="col-md-4 mb-3">
                    <label for="validationCustomUsername">Data de nascimento</label>
                    <div class="input-group">
                        <input type="date" class="form-control" id="validationCustomUsername" value="{{$employee->data_nascimento}}" name="data_nascimento" required>
                    </div>
                </div>
                <div class="col-md-4 mb-3">
                    <label for="validationCustomUsername">Provincia de nascimento</label>
                    <div class="input-group">
                        <input type="text" class="form-control" id="validationCustomUsername" value="{{$employee->provincia_nascimento}}" name="provincia_nascimento" placeholder="Provincia" required>
                    </div>
                </div>
                <div class="col-md-4 mb-3">
                    <label for="validationCustomUsername">Distrito de nascimento</label>
                    <div class="input-group">
                        <input type="texte" class="form-control" id="validationCustomUsername" value="{{$employee->distrito_nascimento}}" name="distrito_nascimento" placeholder="Distrito" required>

                    </div>
                </div>
                <div class="col-md-4 mb-3">
                    <label for="validationCustom03">Nacionalidade</label>
                    <input type="text" class="form-control" id="validationCustom03" value="{{$employee->nacionalidade}}" name="nacionalidade" placeholder="Nacionalidade" required>
                 </div>
            </div>
            <div class="form-row">
                <div class="col-md-3 mb-3">
                    <label for="validationCustom03">Sexo</label>
                    <input type="text" class="form-control" id="validationCustom03" value="{{$employee->sexo}}" name="nacionalidade" placeholder="Nacionalidade" required>
                </div>
                <div class="col-md-3 mb-3">
                    <label for="validationCustom05">BI Nº</label>
                    <input type="text" class="form-control" id="validationCustom05" value="{{$employee->nr_bi}}" name="nr_bi" placeholder="Numero de BI" required>
                    <div class="invalid-feedback">
                        Please provide a valid BI.
                    </div>
                </div>
                <div class="col-md-3 mb-3">
                    <label for="validationCustom05">Passaporte Nº</label>
                    <input type="text" class="form-control" id="validationCustom05" value="{{$employee->nr_passaporte}}" name="nr_passaporte" placeholder="Numero de Passaporte" required>

                </div>
                <div class="col-md-3 mb-3">
                    <label for="validationCustom05">Nuit Nº</label>
                    <input type="text" class="form-control" id="validationCustom05" value="{{$employee->nr_nuit}}" name="nr_nuit" placeholder="Numero de Nuit" required>

                </div>
                <div class="col-md-3 mb-3">
                    <label for="validationCustom05">Contacto</label>
                    <input type="text" class="form-control" id="validationCustom05" value="{{$employee->contacto}}" name="contacto" placeholder="Numero de Contacto" required>

                </div>
                <div class="col-md-3 mb-3">
                    <div class="position-relative form-group">
                        <label for="exampleEmail" class="">Email</label>
                        <input name="email" id="exampleEmail" value="{{$employee->email}}" name="email" placeholder="Email" type="email" class="form-control">
                    </div>

                </div>
            </div>
            <div class="form-group">
                <div>
                    <label class="form-check-label" for="invalidCheck">
                        Voltar
                    </label>

                </div>
            </div>


        </fieldset>
        <a href="{{route('employees.index')}}" class="btn btn-info btn-sm">
            <i class="fa fa-arrow-left"></i>
        </a>

        </form>
     </div>
</div>

@endsection
@include('menu', ['atives' => ['func', 'index_func'],'menus' => ['m_func']])
