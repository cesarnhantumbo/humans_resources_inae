@extends('layouts.admin')


@section('content')

<div class="main-card mb-3 card">
    <div class="card-body">
       <h5>Fucionario</h5>
        <form class="needs-validation" novalidate method="POST" action="{{ route('employees.update', [$employee->id]) }}">
            @csrf
            @method('PUT')
            <div class="form-row">
                <div class="col-md-4 mb-3">
                    <label for="validationCustom01">Nome Completo</label>
                <input type="text" class="form-control" id="validationCustom01" name="nome" value="{{$employee->nome ?? ''}}" placeholder="Nome Completo" required>
                 </div>
                <div class="col-md-4 mb-3">
                    <label for="validationCustom02">Apelido</label>
                    <input type="text" class="form-control" id="validationCustom02" value="{{$employee->apelido ?? ''}}"  name ="apelido" placeholder="Apelido" required>
                </div>
                <div class="col-md-4 mb-3">
                    <label for="validationCustomUsername">Data de nascimento</label>
                    <div class="input-group">
                        <input type="date" class="form-control" id="validationCustomUsername" value="{{$employee->data_nascimento ?? ''}}" name="data_nascimento" required>
                    </div>
                </div>
                <div class="col-md-6 mb-3">
                    <div class="position-relative form-group"><label for="exampleSelect" class="">Provincia de nascimento</label>
                        <select name="provincia_nascimento" id="province_id" class="form-control">
                            <option>Provincia</option>
                            @foreach ($provinces as $province)
                                <option value="{{$province->id}}-{{$province->nome}}">{{$province->nome}}</option>
                            @endforeach
                        </select>
                        <div class="invalid-feedback">
                            Please provide a valid Provincia de nascimento.
                        </div>
                    </div>
                </div>
                <div class="col-md-6 mb-3">
                    <div class="position-relative form-group"><label for="exampleSelect" class="">Distrito de nascimento</label>
                        <select name="distrito_nascimento" id="getdistrict" class="form-control" required>

                        </select>
                        <div class="invalid-feedback">
                            Please provide a valid Distrito de nascimento.
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-6 mb-3">
                    <label for="validationCustom03">Nacionalidade</label>
                    <input type="text" class="form-control" id="validationCustom03" value="{{$employee->nacionalidade ?? ''}}" name="nacionalidade" placeholder="Nacionalidade" required>
                 </div>
                <div class="col-md-3 mb-3">
                    <label for="validationCustom03">Sexo</label>
                    <input type="text" class="form-control" id="validationCustom03" value="{{$employee->sexo ?? ''}}" name="sexo" placeholder="Nacionalidade" required>
                </div>
                <div class="col-md-3 mb-3">
                    <label for="validationCustom05">BI Nº</label>
                    <input type="text" class="form-control" id="validationCustom05" value="{{$employee->nr_bi ?? ''}}" name="nr_bi" placeholder="Numero de BI" required>
                    <div class="invalid-feedback">
                        Please provide a valid BI.
                    </div>
                </div>
                <div class="col-md-3 mb-3">
                    <label for="validationCustom05">Passaporte Nº</label>
                    <input type="text" class="form-control" id="validationCustom05" value="{{$employee->nr_passaporte ?? ''}}" name="nr_passaporte" placeholder="Numero de Passaporte" required>

                </div>
                <div class="col-md-3 mb-3">
                    <label for="validationCustom05">Nuit Nº</label>
                    <input type="text" class="form-control" id="validationCustom05" value="{{$employee->nr_nuit ?? ''}}" name="nr_nuit" placeholder="Numero de Nuit" required>

                </div>
                <div class="col-md-3 mb-3">
                    <label for="validationCustom05">Contacto</label>
                    <input type="text" class="form-control" id="validationCustom05" value="{{$employee->contacto ?? ''}}" name="contacto" placeholder="Numero de Contacto" required>

                </div>
                <div class="col-md-3 mb-3">
                    <div class="position-relative form-group">
                        <label for="exampleEmail" class="">Email</label>
                        <input name="email" id="exampleEmail" value="{{$employee->email ?? ''}}" type="email" class="form-control">
                    </div>

                </div>
            </div>
            {{-- <div class="form-group">
                <div>
                    <label class="form-check-label" for="invalidCheck">
                        Voltar
                    </label>

                </div>
            </div>
            <a href="" class="btn btn-info btn-sm">
                <i class="fa fa-arrow-left"></i>
            </a> --}}
            <button  class="btn btn-primary" type="Voltar">Editar</button>
        </form>
     </div>
</div>

@endsection
@include('menu', ['atives' => ['func', 'index_func'],'menus' => ['m_func']])

@section('js_render')
<script>
    $(document).ready(function(){

        function getDistrict(id){

            $.ajax({
                'url': '/employees/getprovincia/' + id,
                'method': 'get',
                'dataType': 'json',

                success:function(data){

                    if(data.success == true){

                        $('#getdistrict').html('');
                        $('#getdistrict').append('<option>Seleccione o Distrito</option>');

                        var x = data.data.length;
                        var output = '';
                        for (let index = 0; index < x; index++) {
                          output += '<option value="'+data.data[index].nome+'">'+data.data[index].nome+'</option>';
                        }

                        $('#getdistrict').append(output);
                    }
                },
                error:function(data){
                    alert('erro');
                }
            });
        }


        $('#province_id').on('change' , function(){
            var id = $(this).val();
            getDistrict(id);
        });


    });
</script>
@endsection
