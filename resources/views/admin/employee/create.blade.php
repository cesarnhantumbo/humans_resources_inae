@extends('layouts.admin')


@section('content')

<div class="main-card mb-3 card">
    <div class="card-body">
       <h5>Fucionario</h5>
        <form class="needs-validation" novalidate method="POST" action="/employees">
            @csrf
            <div class="form-row">
                <div class="col-md-4 mb-3">
                    <label for="validationCustom01">Nome Completo</label>
                <input type="text" class="form-control" id="validationCustom01" name="nome" value="{{old('nome')}}" placeholder="Nome Completo" required>
                         @error('nome')
                            <span style="color: red">{{$message}}</span>
                        @enderror
                </div>
                <div class="col-md-4 mb-3">
                    <label for="validationCustom02">Apelido</label>
                <input type="text" class="form-control" id="validationCustom02"  name ="apelido" value="{{old('apelido')}}" placeholder="Apelido" required>
                        @error('apelido')
                            <span style="color: red">{{$message}}</span>
                        @enderror
                </div>
                <div class="col-md-4 mb-3">
                    <label for="validationCustomUsername">Data de nascimento</label>

                <input type="date" class="form-control" id="validationCustomUsername" name="data_nascimento" value="{{old('data_nascimento')}}"  required>
                            @error('data_nascimento')
                                <span style="color: red">{{$message}}</span>
                            @enderror

                </div>
                 <div class="col-md-4 mb-6">
                    <div class="position-relative form-group"><label for="exampleSelect" class="">Provincia de nascimento</label>
                    <select name="provincia_nascimento" id="province_id" class="form-control" required>
                            <option>Provincia</option>
                            @foreach ($provinces as $province)
                                <option value="{{$province->id}}-{{$province->nome}}">{{$province->nome}}</option>
                            @endforeach
                        </select>
                            @error('provincia_nascimento')
                                <span style="color: red">{{$message}}</span>
                            @enderror
                    </div>
                </div>
                <div class="col-md-4 mb-6">
                    <div class="position-relative form-group"><label for="exampleSelect" class="">Distrito de nascimento</label>
                    <select name="distrito_nascimento" id="getdistrict" class="form-control" required>

                        </select>
                            @error('distrito_nascimento')
                                <span style="color: red">{{$message}}</span>
                            @enderror
                    </div>
                </div>
                <div class="col-md-4 mb-6">
                    <label for="validationCustom03">Nacionalidade</label>
                <input type="text" class="form-control" id="validationCustom03" name="nacionalidade" value="{{old('nacionalidade')}}" placeholder="Nacionalidade" required>
                        @error('nacionalidade')
                            <span style="color: red">{{$message}}</span>
                        @enderror
                </div>
            </div>
            <div class="form-row">

                <div class="col-md-3 mb-3">
                    <div class="position-relative form-group"><label for="exampleSelect" class="">Sexo</label>
                    <select name="sexo" value="{{old('sexo')}}" id="exampleSelect" class="form-control">
                            <option>Masculino</option>
                            <option>Femenino</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-3 mb-3">
                    <label for="validationCustom05">BI Nº</label>
                <input type="text" class="form-control" id="validationCustom05" name="nr_bi" value="{{old('nr_bi')}}" placeholder="Numero de BI" required>
                         @error('nr_bi')
                            <span style="color: red">{{$message}}</span>
                        @enderror
                </div>
                <div class="col-md-3 mb-3">
                    <label for="validationCustom05">Passaporte Nº</label>
                <input type="text" class="form-control" id="validationCustom05" name="nr_passaporte" value="{{old('nr_passaporte')}}" placeholder="Numero de Passaporte" required>
                         @error('nr_passaporte')
                            <span style="color: red">{{$message}}</span>
                        @enderror
                </div>
                <div class="col-md-3 mb-3">
                    <label for="validationCustom05">Nuit Nº</label>
                <input type="text" class="form-control" id="validationCustom05" name="nr_nuit" value="{{old('nr_nuit')}}" placeholder="Numero de Nuit" required>
                        @error('nr_nuit')
                            <span style="color: red">{{$message}}</span>
                        @enderror
                </div>
                <div class="col-md-4 mb-3">
                    <label for="validationCustom05">Contacto</label>
                <input type="text" class="form-control" id="validationCustom05" name="contacto" value="{{old('contacto')}}" placeholder="Numero de Contacto" required>
                         @error('contacto')
                            <span style="color: red">{{$message}}</span>
                        @enderror
                </div>
                <div class="col-md-4 mb-3">
                    <div class="position-relative form-group">
                        <label for="exampleEmail" class="">Email</label>
                    <input name="email" value="{{old('email')}}" id="exampleEmail" placeholder="Email" type="email" class="form-control" required>
                        </div>
                            @error('email')
                                <span style="color: red">{{$message}}</span>
                            @enderror
                </div>
            </div>

            <button class="btn btn-primary" type="submit">Salvar</button>

        </form>

        <script>
            // Example starter JavaScript for disabling form submissions if there are invalid fields
            (function() {
                'use strict';
                window.addEventListener('load', function() {
                    // Fetch all the forms we want to apply custom Bootstrap validation styles to
                    var forms = document.getElementsByClassName('needs-validation');
                    // Loop over them and prevent submission
                    var validation = Array.prototype.filter.call(forms, function(form) {
                        form.addEventListener('submit', function(event) {
                            if (form.checkValidity() === false) {
                                event.preventDefault();
                                event.stopPropagation();
                            }
                            form.classList.add('was-validated');
                        }, false);
                    });
                }, false);
            })();
        </script>
    </div>
</div>


@endsection
@include('menu', ['atives' => ['func', 'create_func'],'menus' => ['m_func']])

@section('js_render')
<script>
    $(document).ready(function(){

        function getDistrict(id){

            $.ajax({
                'url': '/employees/getprovincia/' + id,
                'method': 'get',
                'dataType': 'json',

                success:function(data){

                    if(data.success == true){

                        $('#getdistrict').html('');
                        $('#getdistrict').append('<option>Seleccione o Distrito</option>');

                        var x = data.data.length;
                        var output = '';
                        for (let index = 0; index < x; index++) {
                          output += '<option value="'+data.data[index].nome+'">'+data.data[index].nome+'</option>';
                        }

                        $('#getdistrict').append(output);
                    }
                },
                error:function(data){
                    alert('erro');
                }
            });
        }


        $('#province_id').on('change' , function(){
            var id = $(this).val();
            getDistrict(id);
        });


    });
</script>
@endsection

