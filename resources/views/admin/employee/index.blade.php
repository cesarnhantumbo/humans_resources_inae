@extends('layouts.admin')

@section('content')
<div class="main-card mb-3 card">
    <div class="card-body"><h5 style = "margin-bottom:2%;" class="card-title">Funcionarios</h5>
        <div class="table-responsive">
            @csrf
            <table class="mb-0 table table-hover">
                <thead class="thead-dark">
                <tr>
                    <th>id</th>
                    <th>nome</th>
                    <th>nacionalidade</th>
                    <th>sexo</th>
                    <th>contacto</th>
                    <th>email</th>
                    <th>Operação</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($employee as $employees)
                    <tr>
                        <th scope="row">{{$employees->id}}</th>
                        <td>{{$employees->nome}}</td>
                        <td>{{$employees->nacionalidade}}</td>
                        <td>{{$employees->sexo}}</td>
                        <td>{{$employees->contacto}}</td>
                        <td>{{$employees->email}}</td>
                        <td>
                            <a href="{{route('employees.show', $employees->id)}}" class="btn btn-info btn-sm">
                                <i class="fas fa-eye"></i>
                            </a>
                            <a href="{{route('employees.edit', $employees->id)}}" class="btn btn-warning btn-sm">
                                <i class="fas fa-pen"></i>
                            </a>
                            <a href="{{route('employees.destroy', $employees->id)}}" class="btn btn-danger btn-sm btn-del">
                                <i class="fas fa-trash"></i>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
</div>
</div>
@endsection

@include('menu', ['atives' => ['func', 'index_func'],'menus' => ['m_func']])

@section('js_render')
<script>

  (function (win,doc) {

      function confirmDel(event) {

          event.preventDefault();
          //console.log(event.target.parentNode.href);
           let token = doc.getElementsByName("_token")[0].value;
           if(confirm("Deseja mesmo apagar?")){
            let ajax = new XMLHttpRequest();
            ajax.open("DELETE",event.target.parentNode.href);
               ajax.setRequestHeader('X-CSRF-TOKEN',token);
               ajax.onreadychange = function(){
                     if(ajax.readyState === 4 && ajax.status === 200){
                        win.location.href ="";

                     }
               };
               ajax.send();


           }else{
                return false;
           }
      }
      if(doc.querySelector('.btn-del')){
          let btn = doc.querySelectorAll('.btn-del');
          for(let i=0; i<btn.length; i++){
              btn[i].addEventListener('click',confirmDel,false);
          }

      }
  })(window,document);

</script>
@endsection
