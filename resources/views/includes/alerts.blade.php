<style>
    .alters{
        position: fixed;
        top: 64px;
        right: 32px;
        z-index: 10;
        opacity: .99;
        transition: all 1s;
    }
    .grids{
        display: grid;
        grid-template-columns: 32px 1fr;
    }
    .alters i {
        font-size: 24px;
    }
</style>
@if (session('success'))

<div class="alert alert-success alters grids"> 
    <div class="d-flex justify-content-center align-content-center mr-2" >
        <i class="fas fa-check"></i> 
    </div>
    <div class="">
        <button type="button" class="close pl-4" data-dismiss="alert">&times;</button>
        {{ session('success') }}
    </div>
</div>

@endif
@if (session('error'))
<div class="alert alert-danger alters grids"> 
    <div class="d-flex justify-content-center align-content-center mr-2" >
        <i class="fas fa-exclamation-triangle"></i> 
    </div>
    <div class="">
        <button type="button" class="close pl-4" data-dismiss="alert">&times;</button>
        {{ session('error') }}
    </div>
</div>

@endif
 @if ($errors->any())
        <div class="alert alert-danger alters">
            <strong>Error!</strong>
  <button type="button" class="close" data-dismiss="alert">&times;</button>
            <hr>
            @foreach ($errors->all() as $item)
                {{ $item }} <br>
            @endforeach
        </div>
@endif 

