<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();


Route::get('/home', 'HomeController@index')->name('home');

//Route::get('/admin/employee/add_employee', 'EmployeeController@create')->name('add_employee');

//Route::get('/admin/employee/show_employee','EmployeeController@show_employee')->name('show_employee');

//Route::resource('admin/employee/show_employee/action', 'EmployeeController');

Route::resource('employees', 'EmployeeController');

//Route::put('employees/{id}', 'EmployeeController@edit');
Route::get('employees/getprovincia/{id}', 'EmployeeController@getDistrito');

