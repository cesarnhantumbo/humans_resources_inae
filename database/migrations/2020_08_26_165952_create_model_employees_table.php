<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateModelEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome');
            $table->string('apelido');
            $table->date('data_nascimento');
            $table->string('provincia_nascimento');
            $table->string('distrito_nascimento');
            $table->string('nacionalidade');
            $table->string('sexo');
            $table->string('nr_bi');
            $table->string('nr_passaporte');
            $table->string('nr_nuit');
            $table->string('contacto');
            $table->string('email',100)->unique();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
