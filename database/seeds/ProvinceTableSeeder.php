<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProvinceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('provinces')->insert([
            'id'             =>  '1',
            'nome'           =>  'Cidade de Maputo',
            'created_at'     =>   now(),


        ]);

        DB::table('provinces')->insert([
            'id'             =>  '2',
            'nome'           =>  'Maputo',
            'created_at'     =>   now(),


        ]);

        DB::table('provinces')->insert([
            'id'             =>  '3',
            'nome'           =>  'Gaza',
            'created_at'     =>   now(),


        ]);
        DB::table('provinces')->insert([
            'id'             =>  '4',
            'nome'           =>  'Inhambane',
            'created_at'     =>   now(),


        ]);
        DB::table('provinces')->insert([
            'id'             =>  '5',
            'nome'           =>  'Manica',
            'created_at'     =>   now(),


        ]);

        DB::table('provinces')->insert([
            'id'             =>  '6',
            'nome'           =>  'Sofala',
            'created_at'     =>   now(),


        ]);
        DB::table('provinces')->insert([
            'id'             =>  '7',
            'nome'           =>  'Tete',
            'created_at'     =>   now(),


        ]);
        DB::table('provinces')->insert([
            'id'             =>  '8',
            'nome'           =>  'Zambézia',
            'created_at'     =>   now(),


        ]);

        DB::table('provinces')->insert([
            'id'             =>  '9',
            'nome'           =>  'Nampula',
            'created_at'     =>   now(),


        ]);
        DB::table('provinces')->insert([
            'id'             =>  '10',
            'nome'           =>  'Niassa',
            'created_at'     =>   now(),


        ]);



        DB::table('provinces')->insert([
            'id'             =>  '11',
            'nome'           =>  'Cabo Delgado',
            'created_at'     =>   now(),


        ]);
    }
}
