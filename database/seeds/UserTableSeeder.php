<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name'              =>  'César Orlando Nhantumbo',
            'email'             =>  'cesarnhantumbo2017@gmail.com',
            'password'          =>   Hash::make('12345678'),
            'remember_token'    =>   now(),
            'created_at'        =>   now()
        ]);
    }
}
