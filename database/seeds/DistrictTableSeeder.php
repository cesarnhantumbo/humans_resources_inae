<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DistrictTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        /**
         * Distritos da Província da Cidade de Maputo
         */

        DB::table('districts')->insert([
            'province_id' => '1',
            'nome'       => 'Distrito Urbano de KaMpfumo',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '1',
            'nome'       => 'Distrito Urbano de Nlhamankulu',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '1',
            'nome'       => 'Distrito Urbano de KaMaxaquene',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '1',
            'nome'       => 'Distrito Urbano de KaMavota',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '1',
            'nome'       => 'Distrito Urbano de KaMubukwana',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '1',
            'nome'       => 'Distrito Municipal de KaTembe',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '1',
            'nome'       => 'Distrito Municipal de KaNyaka',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

          /**
         * Distritos da Província de Maputo
         */

        DB::table('districts')->insert([
            'province_id' => '2',
            'nome'       => 'Boane',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '2',
            'nome'       => 'Magude',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '2',
            'nome'       => 'Manhiça',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '2',
            'nome'       => 'Marracuene',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '2',
            'nome'       => 'Matola',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '2',
            'nome'       => 'Matutuíne',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '2',
            'nome'       => 'Moamba',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '2',
            'nome'       => 'Namaacha',
            'created_at' => now(),
            'updated_at' => now(),
        ]);




        /**
         * Distritos da Província de Gaza
         */

        DB::table('districts')->insert([
            'province_id' => '3',
            'nome'       => 'Bilene',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '3',
            'nome'       => ' Chibuto',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '3',
            'nome'       => 'Chicualacuala',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '3',
            'nome'       => 'Chigubo',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '3',
            'nome'       => 'Chókwè',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '3',
            'nome'       => 'Chongoene',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '3',
            'nome'       => 'Guijá',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '3',
            'nome'       => 'Limpopo',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '3',
            'nome'       => 'Mabalane',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '3',
            'nome'       => 'Manjacaze',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '3',
            'nome'       => 'Mapai',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '3',
            'nome'       => 'Massangena',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '3',
            'nome'       => 'Massingir',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '3',
            'nome'       => 'Xai-Xai',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        /**
         * Distritos de Província de Inhambane
         */

        DB::table('districts')->insert([
            'province_id' => '4',
            'nome'       => 'Funhalouro',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '4',
            'nome'       => 'Govuro',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '4',
            'nome'       => 'Homoíne',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '4',
            'nome'       => 'Inhambane',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '4',
            'nome'       => 'Inharrime',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '4',
            'nome'       => 'Inhassoro',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '4',
            'nome'       => 'Jangamo',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '4',
            'nome'       => 'Mabote',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '4',
            'nome'       => 'Massinga',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '4',
            'nome'       => 'Maxixe',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '4',
            'nome'       => 'Morrumbene',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '4',
            'nome'       => 'Panda',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '4',
            'nome'       => 'Vilanculos',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '4',
            'nome'       => 'Zavala',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

         /**
         * Distritos da Província de Manica
         */

        DB::table('districts')->insert([
            'province_id' => '5',
            'nome'       => 'Bárue',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '5',
            'nome'       => 'Chimoio',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '5',
            'nome'       => 'Gondola',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '5',
            'nome'       => 'Guro',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '5',
            'nome'       => 'Macate',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '5',
            'nome'       => 'Machaze',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '5',
            'nome'       => 'Macossa',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '5',
            'nome'       => 'Manica',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '5',
            'nome'       => 'Mossurize',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '5',
            'nome'       => 'Sussundenga',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '5',
            'nome'       => 'Tambara',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '5',
            'nome'       => 'Vanduzi',
            'created_at' => now(),
            'updated_at' => now(),
        ]);



        /**
         * Distritos da Província de Sofala
         */
        DB::table('districts')->insert([
            'province_id' => '6',
            'nome'       => 'Beira',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '6',
            'nome'       => 'Búzi',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '6',
            'nome'       => 'Caia',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '6',
            'nome'       => 'Chemba',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '6',
            'nome'       => 'Cheringoma',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '6',
            'nome'       => 'Chibabava',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '6',
            'nome'       => 'Dondo',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '6',
            'nome'       => 'Gorongosa',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '6',
            'nome'       => 'Machanga',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '6',
            'nome'       => 'Maringué',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '6',
            'nome'       => 'Marromeu',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '6',
            'nome'       => 'Muanza',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '6',
            'nome'       => 'Nhamatanda',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        /**
         * Distritos da Província de Tete
         */

        DB::table('districts')->insert([
            'province_id' => '7',
            'nome'       => 'Angónia',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('districts')->insert([
            'province_id' => '7',
            'nome'       => 'Cahora-Bassa',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '7',
            'nome'       => 'Changara',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '7',
            'nome'       => 'Chifunde',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '7',
            'nome'       => 'Chiuta',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '7',
            'nome'       => 'Dôa',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '7',
            'nome'       => 'Macanga',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '7',
            'nome'       => 'Magoé',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '7',
            'nome'       => 'Marara',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '7',
            'nome'       => 'Marávia',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '7',
            'nome'       => 'Moatize',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '7',
            'nome'       => 'Mutarara',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '7',
            'nome'       => 'Tete',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '7',
            'nome'       => 'Tsangano',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '7',
            'nome'       => 'Zumbo',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        /**
         * Distritos da Província da Zambézia
         */

        DB::table('districts')->insert([
            'province_id' => '8',
            'nome'       => 'Alto Molócue',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '8',
            'nome'       => 'Chinde',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '8',
            'nome'       => 'Derre',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '8',
            'nome'       => 'Gilé',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '8',
            'nome'       => 'Gurué',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '8',
            'nome'       => 'Ile',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '8',
            'nome'       => 'Inhassunge',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '8',
            'nome'       => 'Luabo',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '8',
            'nome'       => 'Lugela',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '8',
            'nome'       => 'Maganja da Costa',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '8',
            'nome'       => 'Milange',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '8',
            'nome'       => 'Mocuba',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '8',
            'nome'       => 'Mocubela',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '8',
            'nome'       => 'Molumbo',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '8',
            'nome'       => 'Mopeia',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '8',
            'nome'       => 'Morrumbala',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '8',
            'nome'       => 'Mulevala',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '8',
            'nome'       => 'Namacurra',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '8',
            'nome'       => 'Namarroi',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '8',
            'nome'       => 'Nicoadala',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '8',
            'nome'       => 'Pebane',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '8',
            'nome'       => 'Quelimane',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

         /**
         * Distritos da Província de Nampula
         */

        DB::table('districts')->insert([
            'province_id' => '9',
            'nome'       => 'Angoche',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '9',
            'nome'       => 'Eráti',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '9',
            'nome'       => 'Ilha de Moçambique',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '9',
            'nome'       => 'Lalaua',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '9',
            'nome'       => 'Larde',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '9',
            'nome'       => 'Liúpo',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '9',
            'nome'       => 'Malema',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '9',
            'nome'       => 'Meconta',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '9',
            'nome'       => 'Mecubúri',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '9',
            'nome'       => 'Memba',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '9',
            'nome'       => 'Mogincual',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '9',
            'nome'       => 'Mogovolas',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '9',
            'nome'       => 'Moma',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '9',
            'nome'       => 'Monapo',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '9',
            'nome'       => 'Mossuril',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '9',
            'nome'       => 'Muecate',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '9',
            'nome'       => 'Murrupula',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '9',
            'nome'       => ' Nacala-a-Velha',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('districts')->insert([
            'province_id' => '9',
            'nome'       => 'Nacala Porto',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('districts')->insert([
            'province_id' => '9',
            'nome'       => 'Nacarôa',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('districts')->insert([
            'province_id' => '9',
            'nome'       => 'Nampula',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('districts')->insert([
            'province_id' => '9',
            'nome'       => 'Rapale',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('districts')->insert([
            'province_id' => '9',
            'nome'       => 'Ribaué',
            'created_at' => now(),
            'updated_at' => now(),
        ]);



        /**
         * Distritos da Província de Niassa
         */

        DB::table('districts')->insert([
            'province_id' => '10',
            'nome'       => 'Chimbonila',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '10',
            'nome'       => 'Cuamba',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '10',
            'nome'       => 'Lago',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '10',
            'nome'       => 'Lichinga',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '10',
            'nome'       => 'Majune',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '10',
            'nome'       => 'Mandimba',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '10',
            'nome'       => 'Marrupa',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '10',
            'nome'       => 'Maúa',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '10',
            'nome'       => 'Mavago',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '10',
            'nome'       => 'Mecanhelas',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '10',
            'nome'       => 'Mecula',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '10',
            'nome'       => 'Metarica',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '10',
            'nome'       => 'Muembe',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '10',
            'nome'       => 'Ngauma',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '10',
            'nome'       => 'Nipepe',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '10',
            'nome'       => 'Sanga',
            'created_at' => now(),
            'updated_at' => now(),
        ]);


        /**
         * Distritos da Província de Cabo Delgado
         */
        DB::table('districts')->insert([
            'province_id' => '11',
            'nome'       => 'Ancuabe',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('districts')->insert([
            'province_id' => '11',
            'nome'       => 'Balama',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('districts')->insert([
            'province_id' => '11',
            'nome'       => 'Chiúre',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('districts')->insert([
            'province_id' => '11',
            'nome'       => 'Ibo',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('districts')->insert([
            'province_id' => '11',
            'nome'       => 'Macomia',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('districts')->insert([
            'province_id' => '11',
            'nome'       => 'Mecúfi',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('districts')->insert([
            'province_id' => '11',
            'nome'       => 'Meluco',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('districts')->insert([
            'province_id' => '11',
            'nome'       => 'Metuge',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('districts')->insert([
            'province_id' => '11',
            'nome'       => 'Mocímboa da Praia',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('districts')->insert([
            'province_id' => '11',
            'nome'       => 'Montepuez',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('districts')->insert([
            'province_id' => '11',
            'nome'       => 'Mueda',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('districts')->insert([
            'province_id' => '11',
            'nome'       => 'Muidumbe',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('districts')->insert([
            'province_id' => '11',
            'nome'       => 'Namuno',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('districts')->insert([
            'province_id' => '11',
            'nome'       => 'Nangade',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('districts')->insert([
            'province_id' => '11',
            'nome'       => 'Palma',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('districts')->insert([
            'province_id' => '11',
            'nome'       => 'Pemba',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('districts')->insert([
            'province_id' => '11',
            'nome'       => 'Quissanga',
            'created_at' => now(),
            'updated_at' => now(),
        ]);




    }
}
