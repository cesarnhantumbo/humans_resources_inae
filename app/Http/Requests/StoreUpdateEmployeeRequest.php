<?php

namespace App\Http\Requests;

use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;

class StoreUpdateEmployeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
             'nome'=>'required|min:10|max:255',
             'apelido'=>'required|alpha|min:3|max:50',
             'data_nascimento'=>'required|date',
             'provincia_nascimento'=>'required|min:3|max:255',
             'distrito_nascimento'=>'required|min:3|max:255',
             'nacionalidade'=>'required|alpha|min:4|max:255',
             'sexo'=>'required|string|min:1|max:10',
             'nr_bi'=>'required|alpha_num|size:13',
             'nr_passaporte'=>'required|alpha_num|size:9',
             'nr_nuit'=>'required|numeric|digits:9',
             'contacto'=>'required|starts_with:82,83,84,85,86,87|numeric|digits:9|unique:employees,contacto',
             'email'=>'required|email|unique:employees,email|min:10|max:100'
        ];
    }

    public function messages()
    {
        return [
            //'data_nascimento'=>'Precisa informar idade maior de 18 anos',
            'nr_bi.alpha_num'=>'O campo BI Nº só pode conter letras e números.',
            'nr_bi.size'=>'O campo BI Nº deve ter 13 caracteres.',
            'nr_passaport.alpha_num'=>'O campo passaporte Nº só pode conter letras e números.',
            'nr_passaporte.size'=>'O campo passaporte Nº deve ter 9 caracteres',
            'nr_nuit.digitis'=>'O campo Nuit Nº deve ter 9 caracteres',
            'nr_nuit.numeric'=>'O campo Nuit nº deve ser um número.',
            'contacto.digitis'=>'O campo contacto deve ter 9 caracteres',
        ];
    }

}
