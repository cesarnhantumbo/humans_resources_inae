<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreUpdateEmployeeRequest;
use App\Models\ModelDistrict;
use App\Models\ModelEmployee;
use App\Models\ModelProvince;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{

    private $objEmployee;
    private $objProvince;

    public function __construct(){

        $this->objEmployee = new ModelEmployee();
        $this->objProvince = new ModelProvince();

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getDistrito($id)
    {
        $distritos = ModelDistrict::where('province_id', $id)->get();

        return response()->json([
            'success' => true,
            'data' => $distritos
        ]);
    }

    public function index()
    {

        $employee = $this->objEmployee->all();
        return view('admin.employee.index',compact('employee'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $employee=$this->objEmployee->all();
        $provinces = $this->objProvince->all();

        return view('admin.employee.create',compact('employee','provinces'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUpdateEmployeeRequest $request)
    {

        $salvar=$this->objEmployee->create([
            'nome'=>$request->nome,
            'apelido'=>$request->apelido,
            'data_nascimento'=>$request->data_nascimento,
            'provincia_nascimento'=>$request->provincia_nascimento,
            'distrito_nascimento'=>$request->distrito_nascimento,
            'nacionalidade'=>$request->nacionalidade,
            'sexo'=>$request->sexo,
            'nr_bi'=>$request->nr_bi,
            'nr_passaporte'=>$request->nr_passaporte,
            'nr_nuit'=>$request->nr_nuit,
            'contacto'=>$request->contacto,
            'email'=>$request->email
        ]);
        if($salvar){
            return redirect()->route('employees.index')->with('success', 'Adicionado com sucesso!');
        }else{
            return redirect()->back()->with('error', 'nao ... com sucesso!');

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       // echo $id;

      $employee=$this->objEmployee->find($id);

      return view('admin.employee.show',compact('employee'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $employee = $this->objEmployee->find($id);
        $provinces = $this->objProvince->all();

        return view('admin.employee.edit',compact('employee','provinces'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $editar = $this->objEmployee->where(['id'=>$id])->update([
            'nome'=>$request->nome,
            'apelido'=>$request->apelido,
            'data_nascimento'=>$request->data_nascimento,
            'provincia_nascimento'=>$request->provincia_nascimento,
            'distrito_nascimento'=>$request->distrito_nascimento,
            'nacionalidade'=>$request->nacionalidade,
            'sexo'=>$request->sexo,
            'nr_bi'=>$request->nr_bi,
            'nr_passaporte'=>$request->nr_passaporte,
            'nr_nuit'=>$request->nr_nuit,
            'contacto'=>$request->contacto,
            'email'=>$request->email
        ]);
        if($editar){
            return redirect()->route('employees.index')->with('success', 'Editado com sucesso!');
        }else{
            return redirect()->back()->with('error', 'nao ... com sucesso!');

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $del = $this->objEmployee->destroy($id);
        return($del)?"Sim":"Não";
    }
}
