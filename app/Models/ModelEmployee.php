<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModelEmployee extends Model
{
    protected $table ="employees";
    protected $fillable = ['nome','apelido','data_nascimento','provincia_nascimento','distrito_nascimento','nacionalidade','sexo','nr_bi','nr_passaporte','nr_nuit','contacto','email'];
}
